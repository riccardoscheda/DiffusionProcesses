# DiffusionProcesses
In This project i will try to analyze diffusion processes beginning from 
a Monte Carlo simulation the Langevin equation to obtain the statistical
 properties of the system and to obtain a probability density function
 rho(x,t). Then using the rho(x,t) the entropy of the system is computed.  
